import React, {Fragment, useContext} from 'react';
import {Link, NavLink, useHistory} from 'react-router-dom';

/*Context*/
import UserContext from './../UserContext';

/*react bootstrap*/
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
/*or you can use this para di isa isa
import {Nav, Navbar} from 'react-bootstrap';
*/

/*app navbar*/
export default function AppNavbar(){
/*export default function AppNavbar(props){
console.log(props)
let user = props.user*/

  /*    destrcuture context object*/
  const {user, unsetUser} = useContext(UserContext)

  //useHistory is a react-router0dom
  let history = useHistory()

  const logout = () => {
    unsetUser();
    history.push('/login');

  }



//  let leftNav = () ? <statement> :  <statement> ~~~Ternary Operator ~~Conditional Rendering
  let leftNav = (user.id !== null) ?
      (user.isAdmin === true) ?
      <Fragment>
        <Nav.Link as= {NavLink} to="/addCourse" >Add Course</Nav.Link>
        <Nav.Link onClick={logout} >Logout</Nav.Link>
      </Fragment>
      :
      <Fragment>
        <Nav.Link onClick={logout} >Logout</Nav.Link>
      </Fragment>
    :
      (
        <Fragment>
           <Nav.Link as= {NavLink} to="/register" >Register</Nav.Link>
           <Nav.Link as= {NavLink} to="/login" >Login</Nav.Link>
        </Fragment>

        )




  return (
    <Navbar bg="primary" expand="lg">
      <Navbar.Brand as={Link} to="/" >Course Booking</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link as={NavLink} to="/">Home</Nav.Link>
          <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
        </Nav>
        <Nav className="ml-auto"> 
          {leftNav}
        </Nav>
      </Navbar.Collapse>
    </Navbar>

    )
}


