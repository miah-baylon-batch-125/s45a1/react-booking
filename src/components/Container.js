import React from 'react';
import {Link} from 'react-router-dom';

import {
	Container,
	Row,
	Col,
	Jumbotron
} from 'react-bootstrap';

export default function ErrorPage(){


	return (

	<Container fluid className = "text-center">
		<Row>
			<Col className="px-0">
				<Jumbotron fluid className="px-3">
 					<h1>404 - Not Found</h1>
					<p> Page not exist </p>
					<Link to="/">Back to Home Page</Link>
					</Jumbotron>
				</Col>
			</Row>
	</Container>

		)
}