import React from 'react';
import PropTypes from 'prop-types';

/*react bootstrap components*/
import {Card} from 'react-bootstrap';
import {Link} from 'react-router-dom'

export default function Course({courseProp}){
console.log(courseProp)

//let course = courseProp.course //para instead of using props.course.name
	
const {name, description, price, _id} = courseProp

//console.log(name)


//[state, setState] = useState()
/*const [count, setCount] = useState(0);
const [seat, setSeat] = useState(10);
const [isDisabled, setIsDisabled] =useState(false);
*/

//Handler
/*function enroll(){
	//conditional rendering
	if(count < 10 && seat > 0) {
		setSeat(seat - 1)
		setCount(count + 1) 
	} */
/*	
	else {
			alert('No Slots Available.')
			setIsDisabled(true)
		}

*/
	
/*}*/

/*	useEffect( () => {

		if(seat === 0){
			setIsDisabled(true)
		}


	}, [seat])*/

return (
				<Card className="mb-3">
					<Card.Body>
						<Card.Title> {name} </Card.Title>
						<h5>Description:</h5>
						<p> {description} </p>
						<h5>Price:</h5>
						<p> {price} </p>
						{/*<h5>Enrollees</h5>
						<p> {count} Enrollees </p>
						<p> {seat} Seats </p>
				    	<Button variant="primary" onClick={
				    		enroll
				    		()=> {
				    			(count < 30)
				    			? setCount(count + 1)
				    			: alert('No Slots Available')
				    		}
				    	} disabled = {isDisabled} >Enroll</Button>*/}

				    	<Link className="btn btn-primary" to={`/courses/${_id}`} >
				    		Details
				    	</Link>
					</Card.Body>
				</Card>
	)
}


Course.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description:PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}





/*
Using Ternary Operator

{

	(seat > 0)
	?
	setcount(count),

}

*/

/*

Other Option

import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';

import {Card, Button} from 'react-bootstrap';

export default function Course(props){
	console.log(props)
	let course = props.course

	const [seat, setSeat] = useState(10);
	const [isDisabled, setIsDisabled] = useState(false);

	useEffect( () => {
		if(seat === 0){
			setIsDisabled(true)
		}
	}, [seat]);


	return(
		<Card className="mb-3">
			<Card.Body>
				<Card.Title>{course.name}</Card.Title>
				<h5>Description</h5>
				<p>{course.description}</p>
				<h5>Price:</h5>
				<p>{course.price}</p>
				<h5>Seats</h5>
				<p>{seat} Seats</p>
		    	<Button variant="primary" onClick={ 
		    		() => { setSeat( seat - 1)}
		    	} disabled={isDisabled}>Enroll</Button>
			</Card.Body>
		</Card>
	)
}


Course.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}

*/