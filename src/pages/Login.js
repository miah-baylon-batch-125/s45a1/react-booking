import React, {useState, useEffect, useContext} from 'react';

import {Redirect} from 'react-router-dom';

/*Context*/
import UserContext from './../UserContext';

import {Button, Container, Form} from "react-bootstrap";

export default function Login(){
//array destructuring
	const [email, setEmail] =useState('');
	const [password, setPassword] =useState('');
	const [isDisabled, setIsDisabled] =useState(true);

//context object destructuring
	const {user, setUser} =  useContext(UserContext);

	useEffect( () => {

		if(email !== '' && password !== ''){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}

	}, [email, password]);

	function login(e){
		e.preventDefault();

		//alert('Successfully Login!');

		fetch('https://desolate-cliffs-36901.herokuapp.com/api/users/login', 
			{
				method: 'POST',
				headers: {
					"Content-Type": "application/json"
				},
					body: JSON.stringify({
							email: email,
							password: password
					} )
			} )
		.then(result => result.json())
		.then(result => {
			console.log(result) //access token once logged in {access: token}

			if(typeof result.access !== "undefined"){
				//anong gagawin sa access token?
				localStorage.setItem('token', result.access)
				userDetails(result.access)
			}


		})

		const userDetails = (token) => {
			fetch('https://desolate-cliffs-36901.herokuapp.com/api/users/details', {

				method: 'GET',
				headers: {
					'Authorization': `Bearer ${token}`
				}

			} )
			.then(result => result.json())
			.then(result => {
				console.log(result) //buong document ng details ni user

				setUser({
					id: result._id,
					isAdmin: result.isAdmin
				});

			})
		}


		/*update the user using email*/
		//setUser({email: email});

		//save email to local storage   localStorage.setItem(<key>, <value>)
		//localStorage.setItem('email', email)

		setEmail('');
		setPassword('');

	}

	console.log(user);


/*
	if(user.email !== null) {
		return <Redirect to = "/" />
	}
*/

	return(

		(user.id !== null) ? 
			<Redirect to= "/" />

		 : 

			 <Container className = "my-5">
				
				<h1 className="text-center">Login</h1>
				<Form onSubmit={(e) => login(e)} >
					  <Form.Group className="mb-3" controlId="formBasicEmail" onChange = {
					  	(e) => setEmail(e.target.value)  
					  } >
						    <Form.Label>Email address</Form.Label>
						    <Form.Control type="email" placeholder="Enter email" value = {email} />
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="formBasicPassword" onChange = { 	(e) => setPassword(e.target.value)
					}>
						    <Form.Label>Password</Form.Label>
						    <Form.Control type="password" placeholder="Password"  value = {password} />
					  </Form.Group>

					  <Button variant="primary" type="submit" disabled = {isDisabled} >
					    Submit </Button>
				</Form>

			</Container>


	)
}








		