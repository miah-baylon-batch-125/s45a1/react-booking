import React, {useState, useEffect, useContext} from 'react';

import {Button, Container, Form} from "react-bootstrap";
import {Redirect, useHistory} from 'react-router-dom';

import UserContext from './../UserContext';

/*sweetalert*/
import Swal from 'sweetalert2';


export default function Register(){

	const [firstname, setFirstName] = useState('');
	const [lastname, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] =useState('');
	const [password, setPassword] =useState('');
	const [verifyPassword, setVerifyPassword] =useState('');
	const [isDisabled, setIsDisabled] =useState(true);

	const {user} =  useContext(UserContext);
	console.log(UserContext)

	let history = useHistory();


	useEffect( () => {

		if(email !== '' && password !== '' && verifyPassword !== '' && password === verifyPassword ){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}

	}, [email, password, verifyPassword]);



	function register(e){
		e.preventDefault();

		//alert('Registration Successful, you may log in!');

		fetch('https://desolate-cliffs-36901.herokuapp.com/api/users/checkEmail' , 
			{ method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email

			}) 

		})
		.then(result => result.json())
		.then(result =>{

			if(result === true) {
				//alert("User already exist")
				Swal.fire({
					title: 'Duplicate email found',
					icon: 'error',
					text: "Please use another email"

				})
			} else{

				fetch('https://desolate-cliffs-36901.herokuapp.com/api/users/register', 

						{
						 method: "POST",
						headers: {
							"Content-Type": "application/json"
						},
						body: JSON.stringify({
							firstName: firstname,
							lastName: lastname,
							email: email,
							mobileNo: mobileNo,
							password: password

						}) 
					})
				.then(result => result.json())
				.then(result => {
					//console.log(result) //boolean
					if(result === true) {
							Swal.fire({
								title: 'Registration Successful',
								icon: 'success',
								text: "You may login!"
							})
						history.push('/login');
					} else {
								Swal.fire({
								title: 'Something error',
								icon: 'error',
								text: "Try again!"
							})



					}


				} )

			}
		} )

		//setUser({email: email});
		//localStorage.setItem('email', email)

		setFirstName('');
		setLastName('');
		setMobileNo('');

		setEmail('');
		setPassword('');
		setVerifyPassword('');
	}
/*
	if(user.email !== null) {
		unsetUser();
		return <Redirect to = "/login" />
	}
*/


	return(

		(user.id !== null) ?

			<Redirect to="/" />

		: 

			<Container className = "my-5">
			
				<h1 className="text-center">Register</h1>
				<Form onSubmit={ (e) => register(e) } >
				  		<Form.Group className="mb-3" controlId="formBasicFirstName" onChange = {
					  	(e) => setFirstName(e.target.value)  
					  } >
						    <Form.Label>First Name</Form.Label>
						    <Form.Control type="text" placeholder="Enter First Name" value = {firstname} />
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="formBasicLastName" onChange = {
					  	(e) => setLastName(e.target.value)  
					  } >
						    <Form.Label>Last Name</Form.Label>
						    <Form.Control type="text" placeholder="Enter Last Name" value = {lastname} />
					  </Form.Group>

					    <Form.Group className="mb-3" controlId="formBasicMobileNumber" onChange = {
					  	(e) => setMobileNo(e.target.value)  
					  } >
						    <Form.Label>Mobile Number</Form.Label>
						    <Form.Control type="text" placeholder="Enter Mobile Number" value = {mobileNo} />
					  </Form.Group>


					  <Form.Group className="mb-3" controlId="formBasicLastName" onChange = {
					  	(e) => setEmail(e.target.value)  
					  } >
						    <Form.Label>Email address</Form.Label>
						    <Form.Control type="email" placeholder="Enter email" value = {email} />
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="formBasicPassword" onChange = { 	(e) => setPassword(e.target.value)
					}>
						    <Form.Label>Password</Form.Label>
						    <Form.Control type="password" placeholder="Password"  value = {password} />
					  </Form.Group>

				  	  <Form.Group className="mb-3" controlId="formVerifyBasicPassword" onChange = {
				  	  		(e) => setVerifyPassword(e.target.value)
				  	  }>
						    <Form.Label>Verify Password</Form.Label>
						    <Form.Control type="password" placeholder="Verify Password"  value = {verifyPassword} />
					  </Form.Group>

					  <Button variant="primary" type="submit" disabled = {isDisabled}>
					    Submit </Button>
				</Form>

		</Container>

	
	)
}



	